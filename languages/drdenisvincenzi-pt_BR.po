msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2019-02-06 16:24-0200\n"
"PO-Revision-Date: 2019-02-06 16:24-0200\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.6\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Poedit-KeywordsList: __\n"
"X-Poedit-SearchPath-0: drdenisvincenzicombr.php\n"

#: drdenisvincenzicombr.php:73 drdenisvincenzicombr.php:75
#: drdenisvincenzicombr.php:111 drdenisvincenzicombr.php:786
#: drdenisvincenzicombr.php:908
msgid "Testimonials"
msgstr "Depoimentos"

#: drdenisvincenzicombr.php:74 drdenisvincenzicombr.php:76
#: drdenisvincenzicombr.php:110 drdenisvincenzicombr.php:147
#: drdenisvincenzicombr.php:198
msgid "Testimonial"
msgstr "Depoimento"

#: drdenisvincenzicombr.php:77
msgid "Testimonial Archives"
msgstr "Arquivo de Depoimentos"

#: drdenisvincenzicombr.php:78
msgid "Testimonial Attributes"
msgstr "Atributos do Depoimento"

#: drdenisvincenzicombr.php:79
msgid "Parent Testimonial:"
msgstr "Depoimento Pai:"

#: drdenisvincenzicombr.php:80
msgid "All Testimonials"
msgstr "Todos os Depoimentos"

#: drdenisvincenzicombr.php:81
msgid "Add New Testimonial"
msgstr "Adicionar Novo Depoimento"

#: drdenisvincenzicombr.php:82
msgid "Add testimonial"
msgstr "Adicionar novo"

#: drdenisvincenzicombr.php:83
msgid "New Testimonial"
msgstr "Novo Depoimento"

#: drdenisvincenzicombr.php:84
msgid "Edit Testimonial"
msgstr "Editar Depoimento"

#: drdenisvincenzicombr.php:85
msgid "Update Testimonial"
msgstr "Atualizar Depoimento"

#: drdenisvincenzicombr.php:86
msgid "View Testimonial"
msgstr "Ver Depoimento"

#: drdenisvincenzicombr.php:87
msgid "View Testimonials"
msgstr "Ver Depoimentos"

#: drdenisvincenzicombr.php:88
msgid "Search Testimonial"
msgstr "Procurar Depoimento"

#: drdenisvincenzicombr.php:89
msgid "Not found"
msgstr "Não encontrado"

#: drdenisvincenzicombr.php:90
msgid "Not found in Trash"
msgstr "Não encontrado na lixeira"

#: drdenisvincenzicombr.php:91 drdenisvincenzicombr.php:293
#: drdenisvincenzicombr.php:492
msgid "Featured Image"
msgstr "Imagem em destaque"

#: drdenisvincenzicombr.php:92
msgid "Set featured image"
msgstr "Definir imagem em destaque"

#: drdenisvincenzicombr.php:93
msgid "Remove featured image"
msgstr "Remover imagem em destaque"

#: drdenisvincenzicombr.php:94
msgid "Use as featured image"
msgstr "Usar como imagem destacada"

#: drdenisvincenzicombr.php:95
msgid "Insert into testimonial"
msgstr "Inserir no depoimento"

#: drdenisvincenzicombr.php:96
msgid "Uploaded to this testimonial"
msgstr "Carregado para este depoimento"

#: drdenisvincenzicombr.php:97
msgid "Testimonials list"
msgstr "Lista de Depoimentos"

#: drdenisvincenzicombr.php:98
msgid "Testimonials list navigation"
msgstr "Navegação na lista de Depoimentos"

#: drdenisvincenzicombr.php:99
msgid "Filter Testimonials list"
msgstr "Filtrar lista de Depoimentos"

#: drdenisvincenzicombr.php:103
msgid "testimonials"
msgstr "depoimentos"

#: drdenisvincenzicombr.php:160
msgid "Profile picture"
msgstr "Foto do Perfil"

#: drdenisvincenzicombr.php:169 drdenisvincenzicombr.php:302
#: drdenisvincenzicombr.php:501
msgid "Add Image"
msgstr "Adicionar imagem"

#: drdenisvincenzicombr.php:188
msgid "Profile"
msgstr "Perfil"

#: drdenisvincenzicombr.php:225 drdenisvincenzicombr.php:670
#: drdenisvincenzicombr.php:838 drdenisvincenzicombr.php:960
#: drdenisvincenzicombr.php:995
msgid "Hero"
msgstr "Hero"

#: drdenisvincenzicombr.php:237 drdenisvincenzicombr.php:682
#: drdenisvincenzicombr.php:850 drdenisvincenzicombr.php:972
#: drdenisvincenzicombr.php:1007
msgid "Hero Title"
msgstr "Título do Hero"

#: drdenisvincenzicombr.php:247
msgid "Hero Text"
msgstr "Texto do Hero"

#: drdenisvincenzicombr.php:257 drdenisvincenzicombr.php:341
#: drdenisvincenzicombr.php:539
msgid "Button Text"
msgstr "Texto do botão"

#: drdenisvincenzicombr.php:259 drdenisvincenzicombr.php:343
#: drdenisvincenzicombr.php:541
msgid "Know more"
msgstr "Saiba mais"

#: drdenisvincenzicombr.php:268 drdenisvincenzicombr.php:352
#: drdenisvincenzicombr.php:550
msgid "Button URL"
msgstr "URL do botão"

#: drdenisvincenzicombr.php:281
msgid "Clinic"
msgstr "Clínica"

#: drdenisvincenzicombr.php:321
msgid "Clinic Title"
msgstr "Título de Clínica"

#: drdenisvincenzicombr.php:331
msgid "Clinic Text"
msgstr "Texto de Clínica"

#: drdenisvincenzicombr.php:365
msgid "About"
msgstr "Sobre"

#: drdenisvincenzicombr.php:377
msgid "About Title"
msgstr "Título do Sobre"

#: drdenisvincenzicombr.php:392
msgid "About item {#}"
msgstr "Item do Sobre {#}"

#: drdenisvincenzicombr.php:393
msgid "Add Another About item"
msgstr "Adicionar Novo Item do Sobre"

#: drdenisvincenzicombr.php:394
msgid "Remove About item"
msgstr "Remover Item do Sobre"

#: drdenisvincenzicombr.php:403
msgid "Icon"
msgstr "Ícone"

#: drdenisvincenzicombr.php:412
msgid "Add icon"
msgstr "Adicionar ícone"

#: drdenisvincenzicombr.php:432 drdenisvincenzicombr.php:520
msgid "Title"
msgstr "Título"

#: drdenisvincenzicombr.php:441 drdenisvincenzicombr.php:529
msgid "Description"
msgstr "Descrição"

#: drdenisvincenzicombr.php:454
msgid "Procedures"
msgstr "Procedimentos"

#: drdenisvincenzicombr.php:466
msgid "Procedures Title"
msgstr "Título de Procedimentos"

#: drdenisvincenzicombr.php:481
msgid "Procedure {#}"
msgstr "Procedimento {#}"

#: drdenisvincenzicombr.php:482
msgid "Add Another Procedure"
msgstr "Adicionar Novo Procedimento"

#: drdenisvincenzicombr.php:483
msgid "Remove Procedure"
msgstr "Remover Procedimento"

#: drdenisvincenzicombr.php:564
msgid "News"
msgstr "Novidades"

#: drdenisvincenzicombr.php:576
msgid "News Title"
msgstr "Título de Notícias"

#: drdenisvincenzicombr.php:586
msgid "Number of news"
msgstr "Número de notícias"

#: drdenisvincenzicombr.php:587
msgid "Number of news to show"
msgstr "Número de notícias para mostrar"

#: drdenisvincenzicombr.php:605
msgid "Contact"
msgstr "Contato"

#: drdenisvincenzicombr.php:617
msgid "Contact Title"
msgstr "Título do Contato"

#: drdenisvincenzicombr.php:627
msgid "Address"
msgstr "Endereço"

#: drdenisvincenzicombr.php:637
msgid "Phones"
msgstr "Telefones"

#: drdenisvincenzicombr.php:647
msgid "Open hours"
msgstr "Horário de atendimento"

#: drdenisvincenzicombr.php:695 drdenisvincenzicombr.php:863
msgid "Content"
msgstr "Conteúdo"

#: drdenisvincenzicombr.php:707 drdenisvincenzicombr.php:875
msgid "Content Title"
msgstr "Título do Conteúdo"

#: drdenisvincenzicombr.php:717 drdenisvincenzicombr.php:885
msgid "Content Subtitle"
msgstr "Subtítulo do Conteúdo"

#: drdenisvincenzicombr.php:727 drdenisvincenzicombr.php:895
msgid "Content Text"
msgstr "Texto do Conteúdo"

#: drdenisvincenzicombr.php:740
msgid "Gallery"
msgstr "Galeria"

#: drdenisvincenzicombr.php:752
msgid "Gallery images"
msgstr "Imagens da Galeria"

#: drdenisvincenzicombr.php:760
msgid "Add Images"
msgstr "Adicionar imagens"

#: drdenisvincenzicombr.php:761
msgid "Remove Image"
msgstr "Remover imagem"

#: drdenisvincenzicombr.php:762
msgid "File"
msgstr "Arquivo"

#: drdenisvincenzicombr.php:763
msgid "Download"
msgstr "Download"

#: drdenisvincenzicombr.php:764
msgid "Remove"
msgstr "Remover"

#: drdenisvincenzicombr.php:798 drdenisvincenzicombr.php:920
msgid "Show Testimonials"
msgstr "Mostrar Depoimentos"

#: drdenisvincenzicombr.php:799 drdenisvincenzicombr.php:921
msgid "Show Testimonials section on page"
msgstr "Mostrar seção Depoimentos na página"

#: drdenisvincenzicombr.php:808
msgid "Testimonials categories"
msgstr "Categorias dos Depoimentos"

#: drdenisvincenzicombr.php:809
msgid "Testimonials categories to show"
msgstr "Categorias dos depoimentos para exibir"

#~ msgid "Works"
#~ msgstr "Trabalhos"

#~ msgid "Work"
#~ msgstr "Trabalho"

#~ msgid "Work Archives"
#~ msgstr "Arquivo de Trabalhos"

#~ msgid "Work Attributes"
#~ msgstr "Atributos de trabalhos"

#~ msgid "Parent Work:"
#~ msgstr "Trabalho Pai:"

#~ msgid "All Works"
#~ msgstr "Todos os Trabalhos"

#~ msgid "Add New Work"
#~ msgstr "Adicionar Novo Trabalho"

#~ msgid "Add work"
#~ msgstr "Adicionar novo"

#~ msgid "New Work"
#~ msgstr "Novo Trabalho"

#~ msgid "Edit Work"
#~ msgstr "Editar Trabalho"

#~ msgid "Update Work"
#~ msgstr "Atualizar Trabalho"

#~ msgid "View Work"
#~ msgstr "Ver Trabalho"

#~ msgid "View Works"
#~ msgstr "Ver os Trabalhos"

#~ msgid "Search Work"
#~ msgstr "Pesquisar Trabalho"

#~ msgid "Insert into work"
#~ msgstr "Inserir no trabalho"

#~ msgid "Uploaded to this work"
#~ msgstr "Enviado para este trabalho"

#~ msgid "Works list"
#~ msgstr "Lista de Trabalhos"

#~ msgid "Works list navigation"
#~ msgstr "Navegação na lista de Trabalhos"

#~ msgid "Filter Works list"
#~ msgstr "Filtrar lista de Trabalhos"

#~ msgid "works"
#~ msgstr "trabalhos"

#~ msgid "Nextcom Works"
#~ msgstr "Trabalhos da Nextcom"

#~ msgid "Client"
#~ msgstr "Cliente"

#~ msgid "Date"
#~ msgstr "Data"

#~ msgid "Featured Video"
#~ msgstr "Vídeo em destaque"

#~ msgid "Enter a youtube or vimeo URL."
#~ msgstr "Digite um URL do youtube ou vimeo."

#~ msgid "Gallery Text"
#~ msgstr "Texto da Galeria"

#~ msgid "Images"
#~ msgstr "Imagens"

#~ msgid "Use this file"
#~ msgstr "Use este arquivo"

#~ msgid "Use these files"
#~ msgstr "Use estes arquivos"

#~ msgid "File:"
#~ msgstr "Arquivo:"

#~ msgid "Select / Deselect All"
#~ msgstr "Selecionar / Desmarcar tudo"

#~ msgid "Solutions"
#~ msgstr "Soluções"

#~ msgid "Solution"
#~ msgstr "Solução"

#~ msgid "Solution Archives"
#~ msgstr "Arquivo de Soluções"

#~ msgid "Parent Solution:"
#~ msgstr "Solução Pai:"

#~ msgid "All Solutions"
#~ msgstr "Todas as Soluções"

#~ msgid "Add New Solution"
#~ msgstr "Adicionar Nova Solução"

#~ msgid "New Solution"
#~ msgstr "Nova Solução"

#~ msgid "Edit Solution"
#~ msgstr "Editar Solução"

#~ msgid "Update Solution"
#~ msgstr "Atualizar Solução"

#~ msgid "View Solution"
#~ msgstr "Ver Solução"

#~ msgid "View Solutions"
#~ msgstr "Ver Soluções"

#~ msgid "Search Solution"
#~ msgstr "Pesquisar Solução"

#~ msgid "Solutions list"
#~ msgstr "Lista de Soluções"

#~ msgid "solutions"
#~ msgstr "solucoes"

#~ msgid "Nextcom Solutions"
#~ msgstr "Soluções da Nextcom"

#~ msgid "Home"
#~ msgstr "Home"

#~ msgid "Image in Home"
#~ msgstr "Imagem na Home"

#~ msgid "Text in Home"
#~ msgstr "Texto na Home"

#~ msgid "Solution Classes"
#~ msgstr "Classes da Solução"

#~ msgid "Class {#}"
#~ msgstr "Classe {#}"

#~ msgid "Add Another Class"
#~ msgstr "Adicionar Nova Classe"

#~ msgid "Remove Class"
#~ msgstr "Remover Classe"

#~ msgid "Class Image"
#~ msgstr "Imagem da Classe"

#~ msgid "Add Picture"
#~ msgstr "Adicionar Foto"

#~ msgid "Class Name"
#~ msgstr "Nome da Classe"

#~ msgid "Class Description"
#~ msgstr "Descrição da Classe"

#~ msgid "Solution Title"
#~ msgstr "Título da Solução"

#~ msgid "Description Title"
#~ msgstr "Título da Descrição"

#~ msgid "Portfolio Title"
#~ msgstr "Título do Portfólio"

#~ msgid "Catchphrase"
#~ msgstr "Frase de Efeito"

#~ msgid "Catchphrase Author"
#~ msgstr "Autor(a) da Frase de Efeito"

#~ msgid "Contact CTA"
#~ msgstr "Contato CTA"

#~ msgid "Portfolio"
#~ msgstr "Portfólio"

#~ msgid "Footer"
#~ msgstr "Rodapé"

#~ msgid "Footer Text"
#~ msgstr "Texto do Rodapé"

#~ msgid "Footer Contact"
#~ msgstr "Contatos do Rodapé"

#~ msgid "E-mail"
#~ msgstr "E-mail"

#~ msgid "Services"
#~ msgstr "Serviços"

#~ msgid "Service {#}"
#~ msgstr "Serviço {#}"

#~ msgid "Company"
#~ msgstr "Empresa"

#~ msgid "Text"
#~ msgstr "Texto"

#~ msgid "Team"
#~ msgstr "Time"

#~ msgid "Team Title"
#~ msgstr "Título do Time"

#~ msgid "Team Member {#}"
#~ msgstr "Membro do Time {#}"

#~ msgid "Add Another Team Member"
#~ msgstr "Adicionar Novo Membro do Time"

#~ msgid "Remove Team Member"
#~ msgstr "Remover Membro do Time"

#~ msgid "Name"
#~ msgstr "Nome"

#~ msgid "Occupation"
#~ msgstr "Ocupação"

#~ msgid "Testimonial {#}"
#~ msgstr "Depoimento {#}"

#~ msgid "Add Another Entry"
#~ msgstr "Adicionar outra entrada"

#~ msgid "Remove Entry"
#~ msgstr "Remover entrada"

#~ msgid "Author"
#~ msgstr "Autor(a)"

#~ msgid "Solutions Title"
#~ msgstr "Título das Soluções"

#~ msgid "Solution {#}"
#~ msgstr "Solução {#}"

#~ msgid "Solutions CTA"
#~ msgstr "CTA das Soluções"

#~ msgid "Clients"
#~ msgstr "Clientes"

#~ msgid "Client {#}"
#~ msgstr "Cliente {#}"

#~ msgid "Add Another Client"
#~ msgstr "Adicionar Novo Cliente"

#~ msgid "Remove Client"
#~ msgstr "Remover Cliente"

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "Logo"
#~ msgstr "Logo"

#~ msgid "Add Logo"
#~ msgstr "Adicionar Logo"

#~ msgid "Contact Form Title"
#~ msgstr "Título do Formulário de Contato"

#~ msgid "Section Title"
#~ msgstr "Título da Seção"

#~ msgid "contact"
#~ msgstr "contato"

#, fuzzy
#~| msgid "solutions/"
#~ msgid "solutions_cta"
#~ msgstr "solucoes"

#~ msgid "Estúdio Cimbre Works"
#~ msgstr "Trabalhos Estúdio Cimbre"

#~ msgid "No segments found"
#~ msgstr "Nenhum segmento encontrado"

#~ msgid "No segments type"
#~ msgstr "Nenhum tipo de Segmento"

#~ msgid "Segments"
#~ msgstr "Segmentos"

#~ msgid "Segment"
#~ msgstr "Segmento"

#~ msgid "Segment Archives"
#~ msgstr "Arquivo de Segmentos"

#~ msgid "Segment Attributes"
#~ msgstr "Atributos de segmento"

#~ msgid "All Segments"
#~ msgstr "Todos os Segmentos"

#~ msgid "Add segment"
#~ msgstr "Adicionar novo"

#~ msgid "New Segment"
#~ msgstr "Novo Segmento"

#~ msgid "Edit Segment"
#~ msgstr "Editar Segmento"

#~ msgid "View Segment"
#~ msgstr "Ver Segmento"

#~ msgid "View Segments"
#~ msgstr "Ver os Segmentos"

#~ msgid "Insert into segment"
#~ msgstr "Inserir em segmento"

#~ msgid "Uploaded to this segment"
#~ msgstr "Enviado para este segmento"

#~ msgid "Segments list"
#~ msgstr "Lista de Segmentos"

#~ msgid "Segments list navigation"
#~ msgstr "Navegação na lista de Segmentos"

#~ msgid "Filter Segments list"
#~ msgstr "Filtrar lista de Segmentos"

#~ msgid "segments"
#~ msgstr "segmentos"

#~ msgid "Engegroup Segments"
#~ msgstr "Segmentos Engegroup"

#~ msgid "Icon (Color)"
#~ msgstr "Ícone (Cor)"

#~ msgid "Icon (White)"
#~ msgstr "Ícone (Branco)"

#~ msgid "Service"
#~ msgstr "Serviço"

#~ msgid "Service Archives"
#~ msgstr "Arquivo de Serviços"

#~ msgid "Service Attributes"
#~ msgstr "Atributos de serviço"

#~ msgid "Parent Service:"
#~ msgstr "Serviço Pai:"

#~ msgid "All Services"
#~ msgstr "Todos os Serviços"

#~ msgid "Add New Service"
#~ msgstr "Adicionar Novo Serviço"

#~ msgid "New Service"
#~ msgstr "Novo Serviço"

#~ msgid "Edit Service"
#~ msgstr "Editar Serviço"

#~ msgid "Update Service"
#~ msgstr "Atualizar Serviço"

#~ msgid "View Service"
#~ msgstr "Ver Serviço"

#~ msgid "View Services"
#~ msgstr "Ver os Serviços"

#~ msgid "Search Service"
#~ msgstr "Pesquisar Serviço"

#~ msgid "Insert into service"
#~ msgstr "Inserir em serviço"

#~ msgid "Uploaded to this service"
#~ msgstr "Enviado para este serviço"

#~ msgid "Services list navigation"
#~ msgstr "Navegação na lista de Serviços"

#~ msgid "Filter services list"
#~ msgstr "Filtrar lista de Serviços"

#~ msgid "services"
#~ msgstr "serviços"

#~ msgid "Engegroup Services"
#~ msgstr "Serviços Engegroup"

#~ msgid "Engegroup Works"
#~ msgstr "Obras Engegroup"

#~ msgid "Local"
#~ msgstr "Local"

#~ msgid "Services performed"
#~ msgstr "Serviços executados"

#~ msgid "Cases"
#~ msgstr "Cases"

#~ msgid "Case"
#~ msgstr "Case"

#~ msgid "Case Archives"
#~ msgstr "Arquivo de Cases"

#~ msgid "Case Attributes"
#~ msgstr "Atributos de cases"

#~ msgid "Parent Case:"
#~ msgstr "Case Pai:"

#~ msgid "All Cases"
#~ msgstr "Todos os Cases"

#~ msgid "Add New Case"
#~ msgstr "Adicionar Novo Case"

#~ msgid "Add case"
#~ msgstr "Adicionar novo"

#~ msgid "New Case"
#~ msgstr "Novo Case"

#~ msgid "Edit Case"
#~ msgstr "Editar Case"

#~ msgid "Update Case"
#~ msgstr "Atualizar Case"

#~ msgid "View Case"
#~ msgstr "Ver Case"

#~ msgid "View Cases"
#~ msgstr "Ver os Cases"

#~ msgid "Search Case"
#~ msgstr "Pesquisar Case"

#~ msgid "Insert into case"
#~ msgstr "Inserir em case"

#~ msgid "Uploaded to this case"
#~ msgstr "Enviado para este case"

#~ msgid "Cases list"
#~ msgstr "Lista de Cases"

#~ msgid "Cases list navigation"
#~ msgstr "Navegação na lista de Cases"

#~ msgid "Filter Cases list"
#~ msgstr "Filtrar lista de Cases"

#~ msgid "projects/cases"
#~ msgstr "projetos/cases"

#~ msgid "Engegroup Cases"
#~ msgstr "Cases Engegroup"

#~ msgid "Logo (Color)"
#~ msgstr "Logo (Cor)"

#~ msgid "Logo (White)"
#~ msgstr "Logo (Branco)"

#~ msgid "Photos"
#~ msgstr "Fotos"

#~ msgid "cases"
#~ msgstr "cases"

#~ msgid "Color Icon"
#~ msgstr "Ícone colorido"

#~ msgid "White Icon"
#~ msgstr "Ícone branco"

#~ msgid "Color Logo"
#~ msgstr "Logo (Cor)"

#~ msgid "Color logo version"
#~ msgstr "Logo em cores"

#~ msgid "White Logo"
#~ msgstr "Logo (Branco)"

#~ msgid "Sector"
#~ msgstr "Setor"

#~ msgid "Add New"
#~ msgstr "Adicionar Novo(a)"
